nama = "Budi Santoso"
matpel = "Matematika"
nilai = 59.55

#uji grade dengan IF multi kondisi
if(nilai >= 85 and nilai <= 100):
  grade = "A"
elif(nilai >= 75 and nilai < 85):
  grade = "B"
elif(nilai >= 60 and nilai < 75):
  grade = "C"
elif(nilai >= 30 and nilai < 60):
  grade = "D"
else:
  grade = "E"

#cetak data
print("Nama siswa \t:", nama,
      "\nMata Pelajaran \t:",matpel,
      "\nNilai \t\t :", nilai,
      "\ngrade \t\t: ", grade,)