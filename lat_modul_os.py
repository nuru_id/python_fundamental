import os
import latihan12_modul

os.system('cls')

def panggil():
  print('\nOperasi Aritmatika\n'
        'tambah, kurang, kali, bagi, pangkat\n'
        '\nKetik "q" untuk mengakhiri program\n')

  pilih = input("Pilih Operasi Aritmatika yang Anda inginkan ?")

  if pilih =="tambah":
      a = int(input("Masukan Bilangan pertama: "))
      b = int(input("Masukan Bilangan kedua: "))
      latihan12_modul.tambah(a,b)
  elif pilih =="kurang":
      a = int(input("Masukan Bilangan pertama: "))
      b = int(input("Masukan Bilangan kedua: "))
      latihan12_modul.kurang(a,b)
  elif pilih =="kali":
      a = int(input("Masukan Bilangan pertama: "))
      b = int(input("Masukan Bilangan kedua: "))
      latihan12_modul.kali(a,b)
  elif pilih =="bagi":
      a = int(input("Masukan Bilangan pertama: "))
      b = int(input("Masukan Bilangan kedua: "))
      latihan12_modul.bagi(a,b)
  elif pilih =="pangkat":
      a = int(input("Masukan Bilangan pertama: "))
      b = int(input("Masukan Bilangan kedua: "))
      latihan12_modul.pangkat(a,b)
  elif pilih == "q":
    exit()
  else:
    print("coba lagi")

# main
if __name__ == "__main__":
  while True:
    panggil()