
from bs4 import BeautifulSoup

soup = BeautifulSoup("<html><p>Ini paragraf di halaman web</p></html>","html.parser")
print(soup)
print('__'*25)
#<html><p>ini paragraf di halaman web</p></html>

soup = BeautifulSoup("<html><p>ini paragraf di halaman web</p></html>","lxml")
print(soup)
#<html><p>ini paragraf di halaman web</p></html>
print('__'*25)

soup = BeautifulSoup("<html><p>ini paragraf di halaman web</p></html>","xml")
print(soup)
#<html><p>ini paragraf di halaman web</p></html>
print('__'*25)

soup = BeautifulSoup("<html><p>ini paragraf di halaman web</p></html>","html5lib")
print(soup)
#<html><p>ini paragraf di halaman web</p></html>
print('__'*25)