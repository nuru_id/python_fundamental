#deklarasi dan inisialisasi variabel
pelanggan = "Budi Santoso"
totalBelanja = 150000

#struktur kendali if
if(totalBelanja > 100000):
  keterangan = "Selamat Anda mendapat hadiah"
else:
  keterangan = "Terima kasih sudah berbelanja"

#cek data
print("Pelanggan", pelanggan, "\nTotal belanja Anda Rp.",totalBelanja, 
      "\n", keterangan)