#membuat class
class Bank:
  #member1 variable
  norek = ""
  nama = ""
  saldo = 0

  #member2 konstruktor
  def __init__(self, no, nasabah, saldo):
    self.norek = no
    self.nama = nasabah
    self.saldo = saldo
  
  #member3 fungsi
  def nabung(self, uang):
    #self.saldo = self.saldo + uang
    self.saldo += uang
  def tarik(self, uang):
    self.saldo -= uang
  def cetak(self):
    print("No. Rekening \t :", self.norek,
          "\nNama Nasabah \t :", self.nama,
          "\nSaldo \t\t : Rp.", self.saldo,
          "\n----------------------------")    