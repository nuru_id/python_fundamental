#siswa dinyatakan lulus minimal 60 nilainya
nama = "Budi santoso"
matpel  = "Matematika"
nilai = 59.99

#tenary
keterangan = "Lulus" if nilai >= 60 else "Gagal"

#cetak data
print("Nama siswa \t:", nama,
      "\nMata Pelajaran \t:",matpel,
      "\nNilai \t\t :", nilai,
      "\nKeterangan \t: ", keterangan)
