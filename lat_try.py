#python code to illustrate
#working og try()
def devide(x, y):
  try:
    #floor division : give only fractional
    #part as answer
    result = x // y
  except ZeroDivisionError:
    print("Sorry ! you are dividing by zero ")
  else:
    print("Yeah ! your answer is: ", result)
  finally:
    #this block is always executed
    #regardless of exception generation
    print('This is always executed')
#look at parameters and note the working of program
devide(3, 2)
devide(3, 0)
