import tkinter as tk
from tkinter import filedialog, Text
import os

root = tk.Tk()
apps = []

if os.path.isfile('save.txt'):
  with open('save.txt', 'r') as f:
    tempApps = f.read()
    tempApps = tempApps.split(',')
    apps = [x for x in tempApps if x.strip()]
    print(tempApps)

def openfile():
  # menghapus widget pada frame
  for widget in frame.winfo_children():
    widget.destroy()

  filename=filedialog.askopenfilename(initialdir="/", title="select file", filetypes=(("executables",".exe"),("All Files","*.*")))
  apps.append(filename)
  print(filename)
  for app in apps:
    #membuat label
    label = tk.Label(frame, text=app, bg="blue")
    label.pack()

# run app
def runapp():
  for app in apps:
      os.startfile(app)

# Membuat Canvas
canvas = tk.Canvas(root, height=400, width=400, bg="#263d24")
canvas.pack()

# Membuat Frame
frame = tk.Frame(root, bg="white")
frame.place(relwidth=0.8, relheight=0.8, relx=0.1, rely=0.1)

# Membuat Button
openFile = tk.Button(root, text="Open File", padx=10, pady=5, fg="white", bg="#263d42", command=openfile)
openFile.pack()

# Membuat Button
runApp = tk.Button(root, text="Run", padx=10, pady=5, fg="white", bg="#263d42", command=runapp)
runApp.pack()


root.mainloop()

#simpan history ke file
with open('save.txt', 'w') as f:
  for app in apps:
    f.write(app + ',')