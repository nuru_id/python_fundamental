#list
ar_buah = ['Pepaya', 'Mangga', 'Pisang', 'Jambu', 'Belimbing']

for buah in ar_buah : 
  print("Buah", buah)

print("-----------------------------------\n")
#cetak sebuah element array dengan pangil key / index
ar_buah[2] = 'Apel' #ganti element list
print("buah index 2 = ", ar_buah[2])

print("-----------------------------------\n")

del ar_buah[4] #hapus element list

ar_buah.append('Naga')
print("Buah index 4 = ", ar_buah[4])

print("-----------------------------------\n")

#cetak seluruh list buah
for buah in ar_buah : 
  print("Buah", buah)