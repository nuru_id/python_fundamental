import pymysql

db = pymysql.connect(
  host="localhost",
  user="root",
  passwd="",
  database="python_db"
)
cursor = db.cursor()
sql = "INSERT INTO barang (nama, harga) VALUES (%s, %s)"
values = [("Bangku", 50000),("Meja",200000)]

for val in values:
  cursor.execute(sql,val)
  db.commit()

print("{} data berhasil ditambahkan!".format(len(values)))