import pymysql

db = pymysql.connect(
  host="localhost",
  user="root",
  passwd="",
  database="python_db"
)
cursor = db.cursor()
sql = """CREATE TABLE barang (
      id INT AUTO_INCREMENT PRIMARY KEY,
      nama VARCHAR(45),
      harga DOUBLE 
    )"""
cursor.execute(sql)
print("Table berhasil dibuat!")