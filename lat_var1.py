nama = "Budi Santoso"
gender = 'L'
umur = 20
pekerjaan = 'Mahasiswa'
print("Nama Mahasiswa:",nama,
      "\nJenis Kelamin : ",gender,
      "\nUmur: ",umur, " tahun", 
      "\nPekerjaan :",pekerjaan)

print("\nTipe data dari var nama :",type(nama))
print("\nTipe data dari var gender:",type(gender))
print("\nTipe data dari var umur:",type(umur))
print("\nTipe data dari var pekerjaan:",type(pekerjaan))