print('\n--gunakan module dengan memanggil fungsinya--')
from latihan12_modul import tambah, kurang
tambah(20,30)
kurang(2,3)

print('\n---gunakan module dengan memanggil seluruh fungsinya--')
from latihan12_modul import *
tambah(20,30)
kurang(2,3)
kali(5,6)
bagi(20,2)
pangkat(2,3)