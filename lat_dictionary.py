nilai = {'Firda':89, 'Inaya':100, 'Deden':59, 'Fawwaz':95}
print("Data nilai: ", nilai)
print("\n--------cetak value saja--------\n")
# cetak nilai saja
for skor in nilai.values():
  print("Data Nilai :", skor)
print("-----------------------------------\n")
for nama in nilai.keys():
  print("Data Siswa:", nama)
print("-----------------------------------\n")
for nama,skor in nilai.items():
  print("Nama Siswa : %s \t Nilai : %i" % (nama,skor))