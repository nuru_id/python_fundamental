import pymysql

db = pymysql.connect(
  host="localhost",
  user="root",
  passwd="",
  database="python_db"
)
cursor = db.cursor()
sql = "INSERT INTO barang (nama, harga) VALUES (%s, %s)"
val = ("Lemari", 100000)
cursor.execute(sql,val)

db.commit()
print("{} data berhasil ditambahkan!".format(cursor.rowcount))